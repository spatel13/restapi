DROP TABLE IF EXISTS product;

CREATE TABLE product (
  productId VARCHAR(10) NOT NULL,
  productName VARCHAR(100) NOT NULL,
  productPrice DOUBLE
);