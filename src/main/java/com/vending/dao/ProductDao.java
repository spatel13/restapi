package com.vending.dao;

import java.util.List;

import com.vending.model.Product;

public interface ProductDao {
	void insertProduct(Product item);
	void insertProducts(List<Product> products);
	List<Product> getAllProducts();
	Product getProductById(String productId);
	int deleteProduct(String productId);
	int updateProduct(String productId,int productPrice);
}
