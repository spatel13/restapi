package com.vending.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import com.vending.dao.ProductDao;
import com.vending.model.Product;

@Repository
public class ProductDaoImpl extends JdbcDaoSupport implements ProductDao{
	
	@Autowired 
	DataSource dataSource;
	
	@PostConstruct
	private void initialize(){
		setDataSource(dataSource);
	}
	
	@Override
	public void insertProduct(Product item) {
		String sql = "INSERT INTO product " +"(productId, productName, productPrice) VALUES (?, ?, ?)";
		getJdbcTemplate().update(sql, new Object[]{item.getProductId(), item.getProductName()
		});
	}
	
	@Override
	public void insertProducts(final List<Product> products) {
		String sql = "INSERT INTO product " +"(productId, productName, productPrice) VALUES (?, ?, ?)";
		getJdbcTemplate().batchUpdate(sql, new BatchPreparedStatementSetter() {
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				Product product = products.get(i);
				ps.setString(1, product.getProductId());
				ps.setString(2, product.getProductName());
			}
			
			public int getBatchSize() {
				return products.size();
			}
		});

	}
	@Override
	public List<Product> getAllProducts(){
		String sql = "SELECT * FROM product";
		List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql);
		
		List<Product> result = new ArrayList<Product>();
		for(Map<String, Object> row:rows){
			Product product = new Product();
			product.setProductId((String)row.get("productId"));
			product.setProductName((String)row.get("productName"));
			product.setProductPrice((double)row.get("productPrice"));
			result.add(product);
		}
		
		return result;
	}

	@Override
	public Product getProductById(String productId) {
		String sql = "SELECT * FROM product WHERE productId = ?";
		return (Product)getJdbcTemplate().queryForObject(sql, new Object[]{productId}, new RowMapper<Product>(){
			@Override
			public Product mapRow(ResultSet rs, int rwNumber) throws SQLException {
				Product product = new Product();
				product.setProductId(rs.getString("productId"));
				product.setProductName(rs.getString("productName"));
				product.setProductPrice(rs.getDouble("productPrice"));
				return product;
			}
		});
	}
	
	 public int deleteProduct(String productId) {
		    String sql = "DELETE FROM product WHERE productId = ?";
		    Object[] params = {productId};
	        int rows = getJdbcTemplate().update(sql, params);
	        return rows;
	    }
	 
	 public int updateProduct(String productId,int productPrice) {
		    String sql = "UPDATE product SET productPrice = ? WHERE productId = ?";
		    Object[] params = {productPrice,productId};
	        int rows = getJdbcTemplate().update(sql, params);
	        return rows;
	    }
}
