package com.vending.controller;

import java.util.List;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.vending.model.Product;


@RestController
public interface ProductController {
	
	public void getProductById(@RequestBody String productId) ;
	public void getAllProducts();
	public void insertProduct(@RequestBody Product product);
	public void insertProducts(@RequestBody List<Product> products);
	public void deleteProductById(@RequestBody String productId) ;
	public void updateProductPrice(@RequestBody String productId,int productPrice);
	

}
