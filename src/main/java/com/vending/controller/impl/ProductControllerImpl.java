package com.vending.controller.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vending.dao.ProductDao;
import com.vending.model.Product;
import com.vending.service.ProductService;

@RestController
public class ProductControllerImpl {

	@Autowired
	private ProductService productService;
	
	@Autowired
	ProductDao productDao;
	
	@PostMapping("/getProductById")
	public void getProductById(@RequestBody String productId) {
		productService.getProductById(productId);
	}
	
	@RequestMapping("/getAllProducts")
	public void getAllProducts(){
		productService.getAllProducts();
	 }
	
	@PostMapping("/insertProduct")
	public void insertProduct(@RequestBody Product product) {
		productService.insertProduct(product);
	}
	
	@PostMapping("/insertListOfProducts")
	public void insertProducts(@RequestBody List<Product> products) {
		productService.insertProducts(products);
	}
	
	@PostMapping("/deleteProductById")
	public void deleteProductById(@RequestBody String productId) {
		productService.deleteProductById(productId);
	}
	
	@PostMapping("/updateProductPrice")
	public void updateProductPrice(@RequestBody String productId,int productPrice) {
		int rows = productDao.updateProduct(productId, productPrice);
		System.out.println(rows + " row(s) deleted.");
	}
	
}
