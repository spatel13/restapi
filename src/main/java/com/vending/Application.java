package com.vending;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import com.vending.model.Product;
import com.vending.service.ProductService;

@SpringBootApplication
public class Application {
	
	@Autowired
	ProductService productService;

	public static void main(String[] args) {
		ApplicationContext context = SpringApplication.run(Application.class, args);
		ProductService productService = context.getBean(ProductService.class);
		
		
		Product product= new Product();
		product.setProductId("1");
		product.setProductName("Fanta");
		product.setProductPrice(9);
		
		Product product1= new Product();
		product1.setProductId("2");
		product1.setProductName("Coke");
		product.setProductPrice(10);
		
		Product product2= new Product();
		product2.setProductId("3");
		product2.setProductName("Cream Soda");
		product.setProductPrice(8);

		//Insert one product
		productService.insertProduct(product);
        
		
		//Insert a List of Products
		List<Product> products = new ArrayList<>();
		products.add(product1);
		products.add(product2);
		productService.insertProducts(products);

		//Get All Products
		productService.getAllProducts();
		
		//Get product by product ID
		productService.getProductById(product1.getProductId());

	}
}
