package com.vending.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.vending.dao.ProductDao;
import com.vending.model.Product;
import com.vending.service.ProductService;

@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	ProductDao productDao;

	@Override
	public void insertProduct(Product product) {
		productDao.insertProduct(product);
	}

	@Override
	public void insertProducts(List<Product> products) {
		productDao.insertProducts(products);
	}

	
	public void getAllProducts() {
		List<Product> products = productDao.getAllProducts();
		for (Product product : products) {
			System.out.println(product.toString());
		}
	}
   
	
	@Override
	public void getProductById(String productId) {
		Product product = productDao.getProductById(productId);
		System.out.println(product.toString());
	}
	
	@Override
	public void deleteProductById(String productId) {
		int rows = productDao.deleteProduct(productId);
		System.out.println(rows + " row(s) deleted.");
	}
	
	@Override
	public void updateProductPrice(String productId,int productPrice) {
		int rows = productDao.updateProduct(productId, productPrice);
		System.out.println(rows + " row(s) deleted.");
	}
	
	

}
