package com.vending.service;

import java.util.List;

import com.vending.model.Product;

public interface ProductService {
		void insertProduct(Product product);
		void insertProducts(List<Product> products);
		void getAllProducts();
		void getProductById(String productid);
		void deleteProductById(String productId);
		public void updateProductPrice(String productId,int productPrice);
	}

