package Application;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

//Spring manage this bean and create an instance of this
@Component
public class HelloService{
	public Map<String,String> retrieveHelloMessage(){
		//Complex Method
			
			Map<String,String> map = new HashMap<>();
			map.put("name", "janak");
			map.put("surname", "patel");
			map.put("age", "29");
			
			 return map;
		
	}
	
	public String name(NameBean name){
		try{
			if(name.getName() != null){
				return "Hello, my name is "+name.getName()+" and I am "+name.getAge()+" years old";
			}else{
				return "Please enter vaild values";
			}
			
		}catch(Exception e){
			return "Invaild Input";
		}
		
			
		
	}
		
		
		
	}
