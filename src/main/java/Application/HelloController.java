package Application;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
	
    //Dependency
	//Inject Here
	//Autowired - process where spring creates bean and injects where needed
	@Autowired
	private HelloService helloService;
	
	
	@RequestMapping("/hello")
	 public Map<String,String> hello(){
		 return helloService.retrieveHelloMessage();
	 }
	
	@PostMapping("/name")
	 public String name(@RequestBody NameBean name){
		
		
		String msg = helloService.name(name);
		
		 return msg;
	 }
}
