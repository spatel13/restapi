FROM maven:3.5.4-jdk-8-alpine as stdbankbuild

RUN mkdir /rest-api
ADD . /rest-api
WORKDIR /rest-api
RUN mvn package


#--------------------------------

# Alpine Linux with OpenJDK JRE
FROM openjdk:8-jre-alpine
RUN apk add --no-cache bash
# copy WAR into image
COPY --from=stdbankbuild /rest-api/target/shruti.restapi-0.0.1-SNAPSHOT.jar /app.jar 
# run application with this command line 
CMD ["/usr/bin/java", "-jar", "/app.jar"]
